//Tabs
function openTab(evt, tabName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tab-content");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tab-links");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();


/////////

function toggleClass(id) {
    document.getElementById(id).classList.toggle('show');
    // document.getElementsByTagName("html")[0].classList.toggle('no-scroll');
}

function close(id) {
    document.getElementById(id).classList.remove('show');
}

function toggleBoxes(id, boxId) {
    var e = document.getElementById(id);
    var b = document.getElementById(boxId);

    if(e.style.display == 'block') {
        e.style.display = 'none';
        b.classList.remove('opened')
    }
    else {
        e.style.display = 'block';
        b.classList.add('opened')
    }
}

function openModal(modalId) {
    document.getElementById(modalId).classList.remove('fade');
    document.getElementsByTagName("html")[0].classList.add('no-scroll');
}

function closeModal(modalId) {
    document.getElementById(modalId).classList.add('fade');
    document.getElementsByTagName("html")[0].classList.remove('no-scroll');
}

function togglePopup() {
    document.getElementById("calculatorPopup").classList.toggle('show');
    document.getElementsByTagName("html")[0].classList.toggle('no-scroll');
}

var favorites = document.querySelectorAll(".product-item .favorite");

favorites.forEach((item) => {
        item.addEventListener('click', () => {
        item.classList.toggle('active')
    });
});


//input not empty

var input = document.querySelectorAll('.def-input--secondary input');


input.forEach((item) => {
    item.addEventListener('input', evt => {
        const value = item.value;

        if (!value) {
            item.dataset.state = '';
            return;
        }

        const trimmed = value.trim();

        if (trimmed) {
            item.dataset.state = 'valid';
        } else {
            item.dataset.state = 'invalid';
        }
    });
});
