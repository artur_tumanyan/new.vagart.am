<div class="search-box flex justify--space-between">
    <div class="def-input def-input--large search-box__input">
        <input type="text" placeholder="Search" />
    </div>
    <button class="def-button def-button--icon">
        <i class="icon-search"></i>
    </button>
</div>