
<form>
    <div class="form__row price-box">
        <p class="label margin-bottom-medium">Цена</p>
        <div id="slider-range" class="price-box__slider margin-bottom-medium" name="rangeInput"></div>
        <div class="group flex align-items--center justify--space-between">
            <div class="flex__item flex align-items--center margin-right-medium">
                <span class="group__label">от</span>
                <div class="def-input def-input--small flex__item">
                    <input  type="number" min=0 max="9900" oninput="validity.valid||(value='0');" id="min_price" />
                </div>
            </div>
            <div class="flex__item flex align-items--center">
                <span class="group__label">до</span>
                <div class="def-input def-input--small flex__item">
                    <input  type="number" min=0 max="10000" oninput="validity.valid||(value='10000');" id="max_price" />
                </div>
            </div>
        </div>
    </div>
    <div class="form__row">
        <label class="switch flex justify--space-between">
            <span class="switch__text label">Товары со скидкой</span>
            <input type="checkbox">
            <span></span>
        </label>
    </div>
    <div class="form__row">
        <label class="switch flex justify--space-between">
            <span class="switch__text label">Бестселлер</span>
            <input type="checkbox" checked>
            <span></span>
        </label>
    </div>
</form>
