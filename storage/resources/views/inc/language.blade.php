<span class="language-box" id="langBox">
    <span class="language-box__selected-item hide-for-sm-mobile" onclick="toggleBoxes('langList', 'langBox')">Հայ</span>
    <div class="language-box__list" id="langList">
         <ul>
            <li class="language-box__item active">Հայ</li>
            <li class="language-box__item">Eng</li>
            <li class="language-box__item">Рус</li>
        </ul>
    </div>
</span>