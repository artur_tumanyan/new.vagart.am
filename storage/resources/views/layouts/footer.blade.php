<footer class="footer bg-light">
    <div class="footer__inner inner-container flex flex--wrap justify--space-between">
        <div class="flex flex--wrap flex__item footer__left">
            <div class="flex__item footer__col">
                <h4 class="font-standard font-primary--bold">Помощь</h4>
                <ul class="footer__links">
                    <li><a href="">Как сделать заказ</a></li>
                    <li><a href="">Доставка</a></li>
                    <li><a href="">Оплата</a></li>
                </ul>
            </div>
            <div class="flex__item footer__col">
                <h4 class="font-standard font-primary--bold">Контакты</h4>
                <ul class="footer__links">
                    <li><a href="mailto: info@vagart.am"> info@vagart.am </a></li>
                    <li><a href="tel: +37411442221">+374 11 44 22 21</a></li>
                    <li><a href="tel: +37498222221">+374 98 22 22 21</a></li>
                </ul>
            </div>
            <div class="flex__item footer__col">
                <h4 class="font-standard font-primary--bold">Часы работы</h4>
                <ul class="footer__links">
                    <li><a href="">Пн-Пт: 10: 00-18: 00</a></li>
                    <li><a href="">Сб: 10: 00-14: 00</a></li>
                    <li><a href="">Вс: закрыты</a></li>
                </ul>
            </div>
            <div class="flex__item footer__col">
                <h4 class="font-standard font-primary--bold">Адрес</h4>
                <ul class="footer__links">
                    <li><a href="">Гарегин Нжде 16 51/1 Армения , Ереван</a></li>
                </ul>
            </div>
        </div>
        <div class="flex__item footer__right">
            <p class="margin-bottom-medium">&copy; 1998 – 2020 «Vagart». Все права защищены.</p>
            @include('inc.social')
        </div>
    </div>
</footer>
