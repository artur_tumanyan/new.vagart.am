<div class="sidebar">
    <h3 class="font-medium font-primary--bold margin-bottom-large-xs">Категория</h3>
    @include('inc.category-list')
    @include('inc.filter')
</div>
