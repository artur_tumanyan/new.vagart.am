<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes(['register' => false]);
Route::get('/vagart-cms', function () {
    if (Auth::guest()){
        return view('/auth/login');
    }else{
        return redirect('/vagart-cms/dashboard');
    }
});
Route::group(['middleware' => ['auth']], function (){
    /**ad-blocks**/
    Route::resource('/ad-blocks', 'Admin\AdBlocksController');
    /**sliders**/
    Route::post('/sliders', 'Admin\SlidersController@filter');
    Route::post('/change-slider-status', 'Admin\SlidersController@changeStatus');
    Route::post('/sliders-edit', 'Admin\SlidersController@edit');
    Route::post('/sliders-store', 'Admin\SlidersController@store');
    Route::post('/sliders-update', 'Admin\SlidersController@update');
    Route::delete('/sliders-delete/{id}', 'Admin\SlidersController@delete');
    /**banners**/
    Route::post('/banners', 'Admin\BannersController@filter');
    Route::post('/change-banner-status', 'Admin\BannersController@changeStatus');
    Route::post('/banners-edit', 'Admin\BannersController@edit');
    Route::post('/banners-store', 'Admin\BannersController@store');
    Route::post('/banners-update', 'Admin\BannersController@update');
    Route::delete('/banners-delete/{id}', 'Admin\BannersController@delete');
    /**socials**/
    Route::post('/socials', 'Admin\SocialsController@filter');
    Route::post('/socials-edit', 'Admin\SocialsController@edit');
    Route::post('/socials-store', 'Admin\SocialsController@store');
    Route::post('/socials-update', 'Admin\SocialsController@update');
    Route::delete('/socials-delete/{id}', 'Admin\SocialsController@delete');
    /**categories**/
    Route::post('/categories', 'Admin\CategoriesController@filter');
    Route::post('/change-category-status', 'Admin\CategoriesController@changeStatus');
    Route::post('/category-edit', 'Admin\CategoriesController@edit');
    Route::post('/categories-store', 'Admin\CategoriesController@store');
    Route::post('/categories-update', 'Admin\CategoriesController@update');
    Route::post('/categories-sort', 'Admin\CategoriesController@sort');
    Route::delete('/categories-delete/{id}', 'Admin\CategoriesController@delete');
    /**pages**/
    Route::post('/pages', 'Admin\PagesController@filter');
    Route::post('/change-page-status', 'Admin\PagesController@changeStatus');
    Route::post('/page-edit', 'Admin\PagesController@edit');
    Route::post('/pages-store', 'Admin\PagesController@store');
    Route::post('/pages-update', 'Admin\PagesController@update');
    Route::delete('/pages-delete/{id}', 'Admin\PagesController@delete');
    /**brands**/
    Route::post('/brands', 'Admin\BrandsController@filter');
    Route::post('/change-brand-status', 'Admin\BrandsController@changeStatus');
    Route::post('/brand-edit', 'Admin\BrandsController@edit');
    Route::post('/brands-store', 'Admin\BrandsController@store');
    Route::post('/brands-update', 'Admin\BrandsController@update');
    Route::post('/brands-sort', 'Admin\BrandsController@sort');
    Route::delete('/brands-delete/{id}', 'Admin\BrandsController@delete');
    /**types**/
    Route::post('/types', 'Admin\TypesController@filter');
    Route::post('/types-edit', 'Admin\TypesController@edit');
    Route::post('/change-type-status', 'Admin\TypesController@changeStatus');
    Route::post('/types-store', 'Admin\TypesController@store');
    Route::post('/types-update', 'Admin\TypesController@update');
    Route::post('/types-sort', 'Admin\TypesController@sort');
    Route::delete('/types-delete/{id}', 'Admin\TypesController@delete');
    /**colors**/
    Route::get('/colors-list', 'Admin\ColorsController@colorsList');
    Route::post('/colors', 'Admin\ColorsController@filter');
    Route::post('/change-color-status', 'Admin\ColorsController@changeStatus');
    Route::post('/colors-edit', 'Admin\ColorsController@edit');
    Route::post('/colors-store', 'Admin\ColorsController@store');
    Route::post('/color-update', 'Admin\ColorsController@update');
    Route::post('/colors-sort', 'Admin\ColorsController@sort');
    Route::delete('/colors-delete/{id}', 'Admin\ColorsController@delete');
    /**appointment**/
    Route::get('/appointments-list', 'Admin\AppointmentsController@appointmentsList');
    Route::post('/appointments', 'Admin\AppointmentsController@filter');
    Route::post('/change-appointment-status', 'Admin\AppointmentsController@changeStatus');
    Route::post('/appointments-edit', 'Admin\AppointmentsController@edit');
    Route::post('/appointments-store', 'Admin\AppointmentsController@store');
    Route::post('/appointment-update', 'Admin\AppointmentsController@update');
    Route::post('/appointments-sort', 'Admin\AppointmentsController@sort');
    Route::delete('/appointments-delete/{id}', 'Admin\AppointmentsController@delete');
    /**products**/
    Route::post('/products-param', 'Admin\ProductsController@param');
    Route::post('/products', 'Admin\ProductsController@filter');
    Route::post('/products-edit/{id}', 'Admin\ProductsController@edit');
    Route::post('/products-store', 'Admin\ProductsController@store');
    Route::post('/products-update/{id}', 'Admin\ProductsController@update');
    Route::post('/products-sort', 'Admin\ProductsController@filter');
    Route::delete('/products-delete/{id}', 'Admin\ProductsController@delete');
//    /**quick-search**/
//    Route::post('/quick-search', 'Admin\QuickSearchController@filter');
//    Route::post('/quick-search-edit/{id}', 'Admin\QuickSearchController@edit');
//    Route::post('/quick-search-store', 'Admin\QuickSearchController@store');
//    Route::post('/quick-search-update/{id}', 'Admin\QuickSearchController@update');
//    Route::post('/quick-search-sort', 'Admin\QuickSearchController@filter');
//    Route::delete('/quick-search-delete/{id}', 'Admin\QuickSearchController@delete');
    /**parameters**/
    Route::post('/parameters', 'Admin\ParametersController@filter');
    Route::post('/parameters-edit/{id}', 'Admin\ParametersController@edit');
    Route::post('/parameters-update/{id}', 'Admin\ParametersController@update');
    Route::post('/parameters-sort', 'Admin\ParametersController@filter');
    Route::delete('/parameters-delete/{id}', 'Admin\ParametersController@delete');
    /**any**/
    Route::get('/vagart-cms/{any}', 'Admin\HomeController@index')->where('any', '.*');
});

Route::get('/page/{alias}', 'Index\HomeController@page');
Route::get('/product/{alias}', 'Index\ProductsController@product');
Route::get('/{locale?}', 'Index\HomeController@index')->where('locale', 'en|ru');

Route::get('/cart', function () {
    return view('pages.cart');
});
Route::get('/simple', function () {
    return view('pages.simple');
});
Route::get('/product-view', function () {
    return view('pages.product-view');
});
Route::get('/product-view', function () {
    return view('pages.product-view');
});
Route::get('/order-info', function () {
    return view('pages.order.order-info');
});
Route::get('/steps', function () {
    return view('pages.order.steps');
});
Route::get('/step-one', function () {
    return view('pages.order.step-one');
});
Route::get('/step-three', function () {
    return view('pages.order.step-three');
});
Route::get('/step-two', function () {
    return view('pages.order.step-two');
});

Route::prefix('/{locale?}')->group(function () {

    Route::get('/page/{alias}', 'Index\HomeController@page')->where('locale', 'ru');
    Route::get('/product/{alias}', 'Index\ProductsController@product')->where('locale', 'ru');

});

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
