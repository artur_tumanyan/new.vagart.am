<div class="navigation" id="navigation">
<!--    <span class="navigation__close"><i class="icon-close"></i></span>-->
    <div class="navigation__scroll scroll">
        <div class="inner-container flex align-items--stretch flex--wrap">
            <div class="flex__item navigation__col">
                <h4 class="font-primary--bold font-medium margin-bottom-medium">Категория</h4>
                <ul class="list">
                    <li class="list__item active"><a href="">Полимерные люки</a></li>
                    <li class="list__item"><a href="">Смесители</a></li>
                    <li class="list__item"><a href="">Системы водоснабжения и отопления PP-R</a></li>
                    <li class="list__item"><a href="">Kанализационные системы ПП</a></li>
                    <li class="list__item"><a href="">Тёплые водяные полы</a></li>
                    <li class="list__item"><a href="">Алюминиевые радиаторы OGINT</a></li>
                    <li class="list__item"><a href="">Плёнка AGAM</a></li>
                </ul>
            </div>
            <div class="flex__item navigation__col">
                <h4 class="font-primary--bold font-medium margin-bottom-medium">Категория</h4>
                <ul class="list">
                    <li class="list__item"><a href="">Смесители</a></li>
                    <li class="list__item"><a href="">Системы водоснабжения и отопления PP-R</a></li>
                    <li class="list__item"><a href="">Kанализационные системы ПП</a></li>
                    <li class="list__item"><a href="">Алюминиевые радиаторы OGINT</a></li>
                    <li class="list__item"><a href="">Плёнка AGAM</a></li>
                </ul>
            </div>
            <div class="flex__item navigation__col show-for-tablet">
                <div class="max-width-large">
                    @include('inc.filter')
                </div>
            </div>
            <div class="flex__item navigation__col hide-for-mobile">
                <div class="navigation__banner bg-cover"  style="background-image: url('/images/4.jpg')"></div>
            </div>
        </div>
    </div>
    <div class="navigation__language show-for-sm-mobile">
        @include('inc.language')
    </div>
</div>
