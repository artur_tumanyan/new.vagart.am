<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>VAGART</title>
    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/fonts/icomoon/style.css" type="text/css">
    <link rel="stylesheet" href="/css/global.css" type="text/css">

    <link rel="stylesheet" type="text/css" href="/libs/slick/slick.css"/>

    <link rel="stylesheet" href="/css/jquery-ui.css" type="text/css" media="all" />
    <script src="/js/jquery.min.js" type="text/javascript"></script>
    <script src="/js/jquery-ui.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="/js/price-rang.js"></script>
</head>
<body id="body" class=""><!-- TODO  add class 'order-page' for order pages-->
<div class="page-container">
    @include('layouts.header')
    @yield('content')
</div>

@include('layouts.footer')
@include('inc.modals.modal')

<script src="/js/main.js"></script>
<script src="/js/selectbox.js"></script>

<script type="text/javascript" src="/libs/slick/slick.min.js"></script>
<script src="/js/sliders.js"></script>

</body>
</html>
